from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Party(models.Model):
    name = models.CharField(max_length=128)

class Membership(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    party = models.ForeignKey(Party, on_delete=models.CASCADE)

class Bill(models.Model):
    party = models.ForeignKey(Party, on_delete=models.CASCADE)
    amount = models.FloatField()

class Charge(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    bill = models.ForeignKey(Bill, on_delete=models.CASCADE)
    amount = models.FloatField()
