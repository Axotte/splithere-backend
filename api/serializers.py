from django.contrib.auth.models import User

from rest_framework import serializers

from .models import Party, Membership, Bill, Charge

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'email', "first_name", "last_name")

class PartySerializer(serializers.ModelSerializer):
    class Meta:
        model = Party
        fields = ("name", )