from django.shortcuts import render

from rest_framework import generics, permissions

from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope, TokenHasScope

from . import models
from . import serializers

# Create your views here.
class PartyList(generics.ListCreateAPIView):
    permission_classes = [permissions.IsAuthenticated, TokenHasReadWriteScope]
    queryset = models.Party.objects.all()
    serializer_class = serializers.PartySerializer