from django.conf.urls import url

from . import views

urlpatterns = [
    url('party/', views.PartyList.as_view()),
]