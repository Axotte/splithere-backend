from rest_framework import serializers
from django.contrib.auth.models import User
#from django.utils.translation import gettext_lazy as _

class RegisterSerializer(serializers.ModelSerializer):
    confirm_password = serializers.CharField()

    def validate(self, data):
        try:
            user = User.objects.filter(username=data.get('username'))
            if len(user) > 0:
                raise serializers.ValidationError('User with that username already exists')
        except:
            pass

        if not data.get('password') or not data.get('confirm_password'):
            raise serializers.ValidationError('Empty password')

        if data.get('password') != data.get('confirm_password'):
            raise serializers.ValidationError('Password mismatch')

        return data

    def create(self, validated_data):
        user = User.objects.create(
            username=validated_data['username'],
        )
        user.set_password(validated_data['password'])
        user.save()

        return user

    class Meta:
        model = User
        fields = ('username', 'password', 'confirm_password', 'is_active')
        #extra_kwargs = {'confirm_password': {'read_only': True}}